import Component from './Component.js';
export default class Menu extends Component {
	genreSelected = null;
	trieSelected = null;
	genres = null;
	trie = [
		{ name: 'Pertinence', slug: '' },
		{ name: 'Note Métacritique', slug: '-metacritic' },
		{ name: 'Date de sortie', slug: '-released' },
	];
	search = '';

	constructor() {
		super('form', [{ name: 'id', value: 'genreSelector' }]);
		fetch(`https://api.rawg.io/api/genres?key=59977653de77490e8c5e2b87030074be`)
			.then(response => response.json())
			.then(data => (this.genres = data.results));
	}

	render() {
		let html = `<${this.tagName} ${this.renderAttributes()}`;
		html += `>
		<label>Genre: </label>
    	<select name="genres">
			<option value="`;

		let posthtml;

		this.genres.forEach(element => {
			html += element.slug + ',';
			posthtml += `<option value="${element.slug}" ${
				this.genreSelected != null && element.slug == this.genreSelected
					? 'selected'
					: ''
			}>${element.name}</option>`;
		});
		console.log(html);
		html = html.slice(0, -1);
		html += (this.genreSelected == null ? 'selected' : '') + `">Tous</option>`;
		html += posthtml;
		html += `</select>
		<label> Trie par: </label>
    	<select name="trie">`;
		this.trie.forEach(element => {
			html += `<option value="${element.slug}" ${
				this.trieSelected != null && element.slug == this.trieSelected
					? 'selected'
					: ''
			}>${element.name}</option>`;
		});
		html += `</select>
		<label> Recherche : </label>
		<input type="text" name="search" placeholder="Entrez ici le nom du jeux souhaité" value="${this.search}">
		<button type="submit">Filtrer</button>`;
		console.log(html);
		return html + `</${this.tagName}>`;
	}
}
