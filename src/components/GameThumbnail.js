import Component from './Component.js';
import Img from './Img.js';
import Favoris from '../pages/Favoris.js';

export default class GameThumbnail extends Component {
	constructor({ name, background_image, metacritic, slug }) {
		const names = name.split(':');
		const word = [new Component('h4', null, names[0])];
		for (let i = 1; i < names.length; i++) {
			word.push(new Component('h4', null, names[i]));
		}
		word.push(
			new Component(
				'h5',
				null,
				metacritic ? `Note Metacritic : ${metacritic} ` : 'Non noté'
			)
		);
		word.push(
			new Component('img', [
				{
					name: 'src',
					value: `${
						Favoris.favoris.getItem(slug)
							? '../images/fav-full.png'
							: '../images/fav-empty.png'
					}`,
				},
				{ name: 'class', value: 'favButton' },
			])
		);
		super('article', { name: 'class', value: 'gameThumbnail' }, [
			new Component(
				'a',
				[
					{ name: 'href', value: `/detail-${slug}` },
					{ name: 'slug', value: slug },
					{ name: 'name', value: name },
				],
				[new Img(background_image), new Component('section', null, word)]
			),
		]);
	}
}
