import Router from './Router';
import GameList from './pages/GameList';
import Details from './pages/Details';
import Team from './pages/Team';
import Favoris from './pages/Favoris';

const gameList = new GameList([]),
	aboutPage = new Team([]),
	detail = new Details([]),
	favoris = new Favoris([]);

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.menuElement = document.querySelector('.mainMenu');
Router.routes = [
	{ path: '/', page: gameList, title: 'La liste' },
	{
		path: '/team',
		page: aboutPage,
		title: `L'Équipe <u class="through">21</u> 42`,
	},
	{ path: '/detail', page: detail, title: 'Details' },
	{ path: '/mes-favoris', page: favoris, title: 'Mes favoris' },
];

document.querySelector(
	'.logo'
).innerHTML += `<small>les jeux c'est la vie</small>`;

window.onpopstate = () => Router.navigate(document.location.pathname, false);
window.onpopstate();
