
import Img from '../components/Img';

export default class Chargement extends Img{

	static render(){
		return `<img class="loading" src="../../images/loading.gif" alt="loading">`;
	}
	
}