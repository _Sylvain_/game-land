import Page from './Page';
import GameThumbnail from '../components/GameThumbnail';
import Chargement from './Chargement';
import Router from '../Router';
import Favoris from './Favoris';

export default class Details extends Page {
	#games;
	name;
	background_image;
	metacritic;
	description;
	platforms;
	genres;
	screenshots;
	slug;

	constructor(games) {
		super('detail'); // on pase juste la classe CSS souhaitée
		this.games = games;
	}

	mount(el, slug) {
		super.mount(el);
		this.slug = slug;
		// appel ajax lorsque l'on affiche la page
		this.element.innerHTML = Chargement.render(); //Pour enlever la superposition enlever le + au +=
		fetch(
			`https://api.rawg.io/api/games/${slug}?key=59977653de77490e8c5e2b87030074be`
		)
			.then(response => response.json())
			.then(element => {
				if (element.detail == 'Not found.') {
					throw new Error();
				}
				Router.titleElement.innerHTML = `<h1>${
					element.name
				}</h1><img class="favButton" src="${
					Favoris.favoris.getItem(this.slug)
						? '../images/fav-full.png'
						: '../images/fav-empty.png'
				}">`;
				this.#games = new GameThumbnail(element);
				this.name = element.name;
				this.background_image = element.background_image;
				this.metacritic = element.metacritic;
				this.description = element.description;
				this.platforms = element.platforms;
				this.genres = element.genres;
				fetch(
					`https://api.rawg.io/api/games/${slug}/screenshots?key=59977653de77490e8c5e2b87030074be`
				)
					.then(response => response.json())
					.then(element => {
						this.screenshots = element.results;
						this.element.innerHTML = this.render();
						const gamesHTML = document.createElement('article');
						gamesHTML.innerHTML = this.#games.render();
						gamesHTML.querySelector('.favButton').attributes.src.value =
							'../images/fav-full.png';
						document
							.querySelector('.favButton')
							.addEventListener('click', e => {
								Router.updateIconFav(gamesHTML.querySelector('article'), e);
							});
					});
			})
			.catch(err => {
				Router.to404();
			});

		//.then(() => (this.element.innerHTML = this.render())); // affichage dans la page
	}

	render() {
		let render = `<article class="DetailsJeu">
		<img class="image" src="${
			this.background_image
		}" alt="Mettre une uri pour une image">
		 <h2>NOTE METACRITIC : </h2>
		<p class="note">${this.metacritic ? this.metacritic + '/100' : 'Non noté'}</p>
		<br/>
		<h2>GENRE : </h2>
		<p class="text">`;

		this.genres.forEach(element => {
			render += element.name + ' ';
		});

		render += `</p>
		<h2> Description : </h2>
		<p class="text"> ${this.description}</p>
		<h2>Liste des plateformes :</h2>
		<ul>`;

		this.platforms.forEach(element => {
			render += `<li>${element.platform.name}</li>`;
		});

		render += `</ul>`;
		this.screenshots.forEach(element => {
			render += `<a href="${element.image}"><img class="icons" src="${element.image}"></a>`;
		});

		render += `</article>`;
		return render;
	}
}
