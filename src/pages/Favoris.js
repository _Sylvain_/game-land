import Page from './Page';
import GameThumbnail from '../components/GameThumbnail';
import Chargement from './Chargement';
import Menu from '../components/Menu';
import Router from '../Router';

export default class Favoris extends Page {
	//En faire un tableau de jeu
	static favoris;

	constructor(favoris) {
		super('details'); // on pase juste la classe CSS souhaitée
		Favoris.favoris = localStorage;
	}

	mount(element) {
		super.mount(element);
		this.element.innerHTML = '';
		if (Favoris.favoris.length != 0) {
			for (let i = 0; i < Favoris.favoris.length; i++) {
				element.innerHTML += Favoris.favoris.getItem(Favoris.favoris.key(i));
			}
			Router.linksElement();
		} else {
			element.innerHTML += '<h1> Aucun Favoris :( </h1>';
		}
	}

	//Set favoris ne fait que ajouter un nouveau jeu
	static addGame(game) {
		if (!Favoris.favoris) Favoris.favoris = localStorage;
		if (
			Favoris.favoris.getItem(
				game.firstChild.attributes.getNamedItem('slug').value
			)
		) {
			Favoris.favoris.removeItem(
				game.firstChild.attributes.getNamedItem('slug').value
			);
			if (document.location.pathname == '/mes-favoris') {
				game.setAttribute('style', 'display:none');
				if (Favoris.favoris.length == 0)
					document.querySelector('.pageContent').innerHTML =
						'<h1> Aucun Favoris :( </h1>';
			}
		} else {
			console.log('Add game');
			//game.querySelector('.favButton').attributes.src.value =
			//'../images/fav-full.png';
			Favoris.favoris.setItem(
				game.firstChild.attributes.getNamedItem('slug').value,
				game.outerHTML
			);
		}
	}
}
