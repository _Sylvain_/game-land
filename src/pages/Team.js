import Page from './Page';
import Router from '../Router';

export default class Team extends Page {
	constructor() {
		super('detail'); // on pase juste la classe CSS souhaitée
	}

	mount(element) {
		super.mount(element);
		this.element.innerHTML = this.render();
		Router.linksElement();
	}

	render() {
		return `
		<div class="laTeam">
			<div>
				<div title="Maxime Boutry" id="max" class="img"></div>
				<p>Maxime Boutry</p>
				<p><i>dit : </i>Le Chrysalide</p>
				<p><i>Mon jeu préféré : </i><a href="/detail-tales-of-xillia" slug="tales-of-xillia" name="Tales of Xillia" class="ourGame">Tales of Xillia</a></p>
			</div>
			<div>
				<div title="Lucas Hottin" id="lucas" class="img"></div>
				<p>Lucas Hottin</p>
				<p><i>dit : </i>The code Annihilator</p>
				<p><i>He is an anime only</i></p><br/><br/><p>Nous aurions éventuellement besoin d'aide pour la partition équitable des points.</p>
			</div>
			<div>
				<div title="Sylvain Camus" class="img" id="syl20"></div>
				<p>Camus Sylvain</p>
				<p><i>dit : </i>Le SJW</p>
				<p><i>Mon jeu préféré : <a href="/detail-subnautica" slug="subnautica" name="subnautica" class="ourGame">Subnautica</a></p>
			</div>
		</div>
		`;
	}
}
