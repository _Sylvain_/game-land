import Page from './Page';
import GameThumbnail from '../components/GameThumbnail';
import Router from '../Router';
import Menu from '../components/Menu';
import Chargement from './Chargement';

export default class GameList extends Page {
	#games;
	menu;

	constructor(games) {
		super('gameList'); // on pase juste la classe CSS souhaitée
		this.menu = new Menu();
		this.games = games;
	}

	mount(element, query) {
		super.mount(element);
		// appel ajax lorsque l'on affiche la page
		if (query) {
			if (query.includes('genres=')) {
				let selected = query.substring(query.indexOf('genres=') + 7);
				if (selected.includes('&')) {
					selected = selected.substring(0, selected.indexOf('&'));
					if (selected.includes(',')) {
						selected = null;
					}
				}
				this.menu.genreSelected = selected;
			} else this.menu.genreSelected = null;
			if (query.includes('ordering=')) {
				let selected = query.substring(query.indexOf('ordering=') + 9);
				this.menu.trieSelected = selected.includes('&')
					? selected.substring(0, selected.indexOf('&'))
					: selected;
			} else this.menu.trieSelected = null;
			if (query.includes('search=')) {
				let selected = query.substring(query.indexOf('search=') + 7);
				this.menu.search = selected.includes('&')
					? selected.substring(0, selected.indexOf('&'))
					: selected;
			} else this.menu.search = '';
		} else {
			this.menu.genreSelected = null;
			this.menu.trieSelected = null;
			this.menu.search = '';
		}
		this.element.innerHTML = Chargement.render(); //Pour enlever la superposition enlever le + au +=
		fetch(
			`https://api.rawg.io/api/games?key=59977653de77490e8c5e2b87030074be&dates=2020-01-01,2034-01-01&metacritic=50,100${
				query ? '&' + query : ''
			}`
		)
			.then(response => response.json())
			.then(data => (this.games = data.results)) //  maj des children
			.then(() => {
				this.element.innerHTML = this.menu.render();
				this.element.innerHTML += this.render();
				Router.linksElement();
				Router.linksForm();
			}); // affichage dans la page
	}
	set games(value) {
		this.#games = value;
		this.children = this.#games.map(game => new GameThumbnail(game));
	}
}
