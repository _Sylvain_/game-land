import Details from './pages/Details';
import Favoris from './pages/Favoris';

export default class Router {
	static titleElement;
	static contentElement;
	static #menuElement;

	static routes = [];

	/**
	 * Indique au Router la balise HTML contenant le menu de navigation
	 * Écoute le clic sur chaque lien et déclenche la méthode navigate
	 * @param element Élément HTML de la page qui contient le menu principal
	 */
	static set menuElement(element) {
		this.#menuElement = element;
		const links = element.querySelectorAll('a');
		links.forEach(link =>
			link.addEventListener('click', event => {
				event.preventDefault();
				this.navigate(event.target.getAttribute('href'));
			})
		);
	}

	/**
	 * Navigue dans l'application
	 * Affiche la page correspondant à `path` dans le tableau `routes`
	 * @param {String} path URL de la page courante
	 * @param {Boolean} pushState active/désactive le pushState (ajout d'une entrée dans l'historique de navigation)
	 */
	static navigate(path, query, pushState = true) {
		if (window.scrollY > 237)
			document.querySelector('.mainMenu').scrollIntoView();
		if (path.split('-')[0] == '/detail') {
			query = path.slice(8);
			path = path.split('-')[0];
		}
		const route = this.routes.find(route => route.path === path);
		console.log(path);
		console.log(route);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			route.page.mount?.(this.contentElement, query);
			// gestion menu (classe "active")
			const menuLink = this.#menuElement.querySelector(
					`a[href="${route.path}"]`
				),
				prevActiveLink = this.#menuElement.querySelector('a.active');
			prevActiveLink?.classList.remove('active');
			menuLink?.classList.add('active');
			if (pushState) {
				window.history.pushState(
					null,
					null,
					query == 'error-404'
						? query
						: path + (path == '/detail' ? '-' + query : '')
				);
				console.log(path);
			}
		} else {
			Router.to404();
		}
	}

	static to404() {
		this.routes.find(route => route.path === '/detail').title = 'ERROR 404';
		Router.navigate('/detail-error-404');
	}

	static linksForm() {
		const form = this.contentElement.querySelector('form');
		form.addEventListener('submit', event => {
			event.preventDefault();
			const query =
				'genres=' +
				event.target[0].form.elements[0].value +
				'&ordering=' +
				event.target[0].form.elements[1].value +
				(event.target[0].form.elements[2].value != '' &&
				event.target[0].form.elements[2].value.length > 2
					? '&search=' + event.target[0].form.elements[2].value
					: '');
			this.navigate('/', query);
		});
	}

	static linksElement(pushState = true) {
		const links = this.contentElement.querySelectorAll('a');
		links.forEach(link =>
			link.addEventListener('click', event => {
				event.preventDefault();
				if (event.target.className == 'favButton') return;
				const element = event.composedPath().find(el => el.localName == 'a');
				this.routes.find(
					route => route.path == '/detail'
				).title = element.getAttribute('name');
				this.navigate(element.getAttribute('href'));
			})
		);
		Router.linksFavoris();
	}

	static linksFavoris() {
		const game = this.contentElement.querySelectorAll('.favButton');
		game.forEach(game =>
			game.addEventListener('click', event => {
				event.preventDefault();

				const jeu = event.composedPath().find(el => el.localName == 'article');
				Router.updateIconFav(jeu, event);
			})
		);
	}

	static updateIconFav(jeu, e) {
		const favB = e.target.attributes.src;
		favB.value == '../images/fav-full.png'
			? (favB.value = '../images/fav-empty.png')
			: (favB.value = '../images/fav-full.png');
		Favoris.addGame(jeu);
	}
}
