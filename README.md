## GAMELAND :

Lancement du projet :

	- Lancez "npm run watch" dans un terminal pour compiler le site 
	- Lancez le server avec "npx serve -s -l 8000"
	- Connectez vous à l'adresse http://localhost:8000 pour rejoindre la page d'acceuil du projet.



## Captures d'écrans du site (dans le dossier screenshots)
Accueil : 
![Accueil](screenshots/Aperçu_Acceuil.png)

Page de détail (dézoom):

![Accueil](screenshots/Aperçu_détails.png)

Page de l'équipe:

![Accueil](screenshots/Aperçu_Equipe.png)

Page des Favoris:

![Accueil](screenshots/Aperçu_Favoris.png)

Résultat d'une recherche

![Accueil](screenshots/Aperçu_Recherche.png)
